from flask import render_template, flash, redirect, request, url_for
from flask_login import current_user, login_user, logout_user, login_required
from app import app, db
from app.forms import LoginForm, CategoryForm, DishForm
from app.models import User, Dishes, Category


@app.route('/')
@app.route('/index')
# @login_required
def index():
    menu = Dishes.query.filter(Dishes.is_active)
    return render_template('index.html', title='Меню кафе', menu=menu)


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        error_login = False
        if user is None:
            flash('Username not found')
            error_login = True
        if user and not user.check_password(form.password.data):
            flash('Incorrect password')
            error_login = True
        if error_login:
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        return redirect(url_for('index'))
    return render_template('login.html', title='Авторизация', form=form)


@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/profile')
@login_required
def profile():
    return render_template('index.html', title="Настройки")


@app.route('/category/<int:id>/delete/', methods=['GET', 'POST'])
@login_required
def category_delete(id=0):
    form = CategoryForm()
    category = Category.query.get(id)
    if not category:
        flash('Не воз-можно удалить категорию! Категория не найдена', 'error')
        return redirect(url_for('category_list'))
    if request.method == 'POST':
        form.name.data = request.form.get('name')
        form.sort_pn.data = request.form.get('sort_pn')
        form.is_active.data = request.form.get('is_active') == 'y'
        if form.validate_on_submit():
            print('delete category with id={}'.format(id))
            db.session.delete(category)
            db.session.commit()
            flash('Категоория удалена', 'success')
        else:
            print("Don't delete category with id={}".format(id))
            flash(f"Не могу удалить категорию с id={id}")
    else:
        # method == 'GET' Выводим форму
        form.id.data = category.id
        form.name.data = category.name
        form.sort_pn.data = category.sort_pn
        form.is_active.data = category.is_active
        form.is_delete = True
        title = 'Категория (удаление)'
        return render_template('category.html', title=title, form=form)
    return redirect(url_for('category_list'))


@app.route('/category/<int:id>/', methods=['GET', 'POST'])
def category_edit(id=0):
    print('function category_edit')
    form = CategoryForm()
    title = ''

    if request.method == 'GET':
        if id == 0:
            print("WOW! It's GET method")
            title = 'Категория (новая)'
        else:
            category = Category.query.get(id)
            title = 'Категория (редактирование)'
            if not category:
                flash('Не найденна такая категория')
                return redirect(url_for('category_list'))
            form.id.data = category.id
            form.name.data = category.name
            form.sort_pn.data = category.sort_pn
            form.is_active.data = category.is_active
            form.is_delete = False
            return render_template('category.html', title=title, form=form)
    else:
        form.name.data = request.form['name']
        form.sort_pn.data = request.form['sort_pn']
        form.is_active.data = request.form.get('is_active') == 'y'

    if form.validate_on_submit():
        if id > 0:
            cat = Category.query.get(id)
            cat.name = form.name.data
            cat.sort_pn = form.sort_pn.data
            cat.is_active = form.is_active.data
        else:
            cat = Category(
                name=form.name.data,
                sort_pn=form.sort_pn.data,
                is_active=form.is_active.data
            )
            db.session.add(cat)
        db.session.commit()
        return redirect(url_for('category_list'))
    return render_template('category.html', title=title, form=form)


@app.route('/category_list')
@login_required
def category_list():
    categories = Category.query.all()
    return render_template('category_list.html', title="Список категорий меню", categories=categories)


@app.route('/dishes_list')
@login_required
def dishes_list():
    arg_filter = request.args.get('filter')
    if not arg_filter:
        arg_filter = '-'
    print('filter={}'.format(arg_filter))
    if arg_filter == '-':
        dishes = Dishes.query.order_by(Dishes.category_id).all()
    else:
        dishes = Dishes.query.filter(Dishes.category_id == arg_filter).order_by(Dishes.category_id)

    data = {'categories': Category.query.all(),
            'dishes': dishes,
            'filter': arg_filter}

    return render_template('dishes_list.html', title="Список блюд меню", data=data)


@app.route('/dishes/<int:id>', methods=['GET', 'POST'])
@login_required
def dish_edit(id=0):
    title = ''
    form = DishForm()

    categories = Category.query.filter(Category.is_active)
    category_list_ch = [(c.id, c.name) for c in categories]
    form.category_selected.choices = category_list_ch

    is_del = request.args.get('del') == 'True'
    if request.method == 'GET':
        # Или новый или редактировать
        if id == 0:
            # новый
            title = 'Новое блюдо'
        else:
            # редактируем существуюшее
            title = 'Блюдо (редактирование)'
            dish = Dishes.query.get(id)
            if not dish:
                flash('Блюдо не найдено')
                return redirect(url_for('dishes_list'))

            form.id.data = dish.id
            form.category_selected.data = dish.category_id
            form.name.data = dish.name
            form.weight.data = dish.weight
            form.measures.data = dish.measures
            form.ingridients.data = dish.ingridients
            form.price.data = round(dish.price, 2)
            form.is_active.data = dish.is_active
            form.sort_pn.data = dish.sort_pn
            form.is_del = is_del
        return render_template('dish.html', title=title, form=form)

    if form.validate_on_submit():
        # Сохраняем
        print('Form validated!!!')
        if id > 0:
            dish = Dishes.query.get(id)
            dish.sort_pn = form.sort_pn.data
            dish.is_active = form.is_active.data
            dish.category_id = form.category_selected.data
            dish.name = form.name.data
            dish.weight = form.weight.data
            dish.measures = form.measures.data
            dish.ingridients = form.ingridients.data
            dish.price = form.price.data
        else:
            print('category_selected={}'.format(form.category_selected.data))
            dish = Dishes(
                name=form.name.data,
                sort_pn=form.sort_pn.data,
                is_active=form.is_active.data,
                category_id=form.category_selected.data,
                weight=form.weight.data,
                measures=form.measures.data,
                ingridients=form.ingridients.data,
                price=form.price.data
            )
            db.session.add(dish)
        db.session.commit()
        return redirect(url_for('dishes_list'))
    print('Form is not validated!!!')
    return render_template('dish.html', title=title, form=form)


@app.route('/dishes/<int:id>/delete/', methods=['GET', 'POST'])
@login_required
def dish_delete(id=0):
    print('dish for delete with id = {}'.format(id))
    form = DishForm()

    dish = Dishes.query.get(id)
    if not dish:
        flash('Не возможно удалить блюдо: Блюдо не найдено')
        return redirect(url_for('dishes_list'))

    if request.method == 'GET':
        title = 'Блюдо (удаление)'
        form.id.data = dish.id
        form.category_selected.data = dish.category_id
        form.name.data = dish.name
        form.weight.data = dish.weight
        form.measures.data = dish.measures
        form.ingridients.data = dish.ingridients
        form.price.data = round(dish.price, 2)
        form.is_active.data = dish.is_active
        form.sort_pn.data = dish.sort_pn
        form.is_delete = True
        return render_template('dish.html', title=title, form=form)

    if form.validate_on_submit():
        print('Deleting dish')
        db.session.delete(dish)
        db.session.commit()
    else:
        print('Deleting dish failed!')

    return redirect(url_for('dishes_list'))
