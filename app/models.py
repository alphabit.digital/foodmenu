from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin
from app import db, login


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    psw_hash = db.Column(db.String(128))

    def __repr__(self):
        return '<User [{}] {}>'.format(self.id, self.username)

    def set_password(self, psw):
        self.psw_hash = generate_password_hash(psw)

    def check_password(self, psw):
        return check_password_hash(self.psw_hash, psw)


class Category(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), default="")
    sort_pn = db.Column(db.Integer, default=100)
    is_active = db.Column(db.Boolean, default=True)
    dishes = db.relationship(
        'Dishes',
        back_populates='category',
        cascade='all, delete',
        passive_deletes=True,
        lazy='dynamic')

    def __repr__(self):
        return '<Category [{}] {}>'.format(self.id, self.name)


class Dishes(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    category_id = db.Column(db.Integer, db.ForeignKey('category.id', ondelete="CASCADE"))
    category = db.relationship("Category", back_populates="dishes")
    name = db.Column(db.String(120), default="")
    weight = db.Column(db.Integer, default=0)
    measures = db.Column(db.String(10), default='гр.')
    ingridients = db.Column(db.String(200), default="")
    price = db.Column(db.Float(10, 2), default=0)
    is_active = db.Column(db.Boolean, default=True)
    sort_pn = db.Column(db.Integer, default=100)

    def __repr__(self):
        return '<Dishes [{}] {}>'.format(self.id, self.name)


@login.user_loader
def load_user(id):
    return User.query.get(int(id))

