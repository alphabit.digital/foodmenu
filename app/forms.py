from flask_wtf import FlaskForm
from wtforms import IntegerField, StringField, PasswordField, FloatField, BooleanField, SubmitField, SelectField, TextAreaField
from wtforms.validators import DataRequired
from app.models import Category


class LoginForm(FlaskForm):
    username = StringField('Имя пользователя', validators=[DataRequired()])
    password = PasswordField('Пароль', validators=[DataRequired()])
    remember_me = BooleanField('Запомнить меня')
    submit = SubmitField('Войти')


class CategoryForm(FlaskForm):
    id = IntegerField('Id')
    name = StringField('Название', validators=[DataRequired()])
    sort_pn = IntegerField('Индекс сортировки', validators=[DataRequired()], default=100)
    is_active = BooleanField('Категория активна (включена)')
    submit = SubmitField('Сохранить')
    submit_del = SubmitField('Удалить')


class DishForm(FlaskForm):
    MEASURES = [('гр.', 'грамм'), ('мл.', 'мл.')]

    # categories = Category.query.filter(Category.is_active)
    # category_list = []
    # for category in categories:
    #     category_list.append((category.id, category.name))

    id = IntegerField('Id', render_kw={'render': 'manual'})
    category_selected = SelectField('Категория', coerce=int, validators=[DataRequired()], render_kw={'render': 'auto'})
    name = StringField('Название', validators=[DataRequired()], render_kw={'render': 'auto'})
    weight = FloatField('Вес/объем', validators=[DataRequired()], default=0, render_kw={'render': 'auto'})
    measures = SelectField('Еденицы измерения', choices=MEASURES, validators=[DataRequired()], default=MEASURES[0], render_kw={'render': 'auto'})
    ingridients = TextAreaField('Ингридиенты', validators=[DataRequired()], render_kw={'render': 'auto'})
    price = FloatField('Цена', validators=[DataRequired()], default=0, render_kw={'render': 'auto'})
    is_active = BooleanField('Блюдо активно (включено)', render_kw={'render': 'manual'})
    sort_pn = IntegerField('Индекс сортировки', validators=[DataRequired()], default=100, render_kw={'render': 'manual'})
    submit = SubmitField('Сохранить', render_kw={'render': 'manual'})
    submit_del = SubmitField('Удалить', render_kw={'render': 'manual'})
