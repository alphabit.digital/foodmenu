from app import app, db
from app.models import User, Dishes, Category


@app.shell_context_processor
def make_shell_context():
    return {'db': db, 'User': User, 'Category': Category, 'Dishes': Dishes}
