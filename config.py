import os
from config_secret import *


basedir = os.path.abspath((os.path.dirname(__file__)))


class Config(object):
    SECRET_KEY = SECRET_KEY

    # database conf
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app_fmc.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    FLASK_ENV = 'development'
    DEBUG = True
